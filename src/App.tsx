import type { Component } from 'solid-js';

import styles from './App.module.css';

const App: Component = () => {
  return (
    <div class={styles.App}>
      <div class={styles.iframeWrapper}>
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=1"
          frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  );
};

export default App;